Description: Uploads an update/install policy to the Jamf Pro Server and creates a Self Service Policy and Smart Group
Identifier: com.adidas.jamf.RStudio
ParentRecipe: com.adidas.pkg.RStudio
MinimumVersion: "2.3"

Input:
  NAME: "RStudio"
  VENDOR_NAME: "RStudio"
  TRIGGER_NAME: "RStudio"
  PACKAGE_CATEGORY: "Applications"
  SCRIPT_CATEGORY: "User Experience"
  SCRIPT_PRIORITY: After
  SELF_SERVICE_ICON: RStudio.png
  SELF_SERVICE_DESCRIPTION: "The RStudio IDE is a set of integrated tools designed to help you be more productive with R and Python. It includes a console, syntax-highlighting editor that supports direct code execution, and a variety of robust tools for plotting, viewing history, debugging and managing your workspace."
  PARAMETER4_GENERAL_INFO: "%VENDOR_NAME%;%NAME%;%version%"
  PARAMETER5_UEX_OPTIONS: "Quit, Block ssavail"
  PARAMETER6_APP_NAME: "%NAME%"
  PARAMETER7_DURATION: "5"

  SMB_SHARE: //hecl0303/dfs/Europe/IT_Europe/hecl0304/UEM-macOS-Auto-Patching

  GROUP_NAME_TARGET: "Application - Installed Version - %NAME%"
  GROUP_NAME_EXCLUSION: "Application - Released Version - %NAME%"
  GROUP_TEMPLATE_TARGET: PolicyTemplate-Target-SmartGroup.xml
  GROUP_TEMPLATE_EXCLUSION: PolicyTemplate-Exclusion-SmartGroup.xml

  POLICY_CATEGORY_UPDATE: "Available Updates"
  POLICY_TEMPLATE_UPDATE: PolicyTemplate-Update.xml
  POLICY_NAME_UPDATE: "aG - %NAME% - Update"
  SCRIPT_NAME_UPDATE: 00-UEX-Update-via-Self-Service.sh
  SELF_SERVICE_DISPLAY_NAME_UPDATE: "%NAME% Update"
  INSTALL_BUTTON_TEXT_UPDATE: Update
  REINSTALL_BUTTON_TEXT_UPDATE: Reinstall

  POLICY_CATEGORY_INSTALL: "Free"
  POLICY_TEMPLATE_INSTALL: PolicyTemplate-Install.xml
  POLICY_NAME_INSTALL: "aG - %NAME% - Install"
  SCRIPT_NAME_INSTALL: 00-UEX-Install-via-Self-Service.sh
  SELF_SERVICE_DISPLAY_NAME_INSTALL: "%NAME% Install"
  INSTALL_BUTTON_TEXT_INSTALL: Get
  REINSTALL_BUTTON_TEXT_INSTALL: Reinstall

  POLICY_CATEGORY_CACHE_TRIGGER: "Manual Cache Trigger"
  POLICY_TEMPLATE_CACHE_TRIGGER: PolicyTemplate-ManualCacheTriggerWithConfig.xml
  POLICY_NAME_CACHE_TRIGGER: "aG - %NAME% - Cache Trigger"

  POLICY_CATEGORY_UPDATE_CACHE_TRIGGER: "Manual Cache Trigger"
  POLICY_TEMPLATE_UPDATE_CACHE_TRIGGER: PolicyTemplate-ManualUpdateCacheTrigger.xml
  POLICY_NAME_UPDATE_CACHE_TRIGGER: "aG - %NAME% - Update Cache Trigger"

  POLICY_CATEGORY_MANUAL_UPDATE_TRIGGER: "Manual Update Trigger"
  POLICY_TEMPLATE_MANUAL_UPDATE_TRIGGER: PolicyTemplate-ManualUpdateTrigger.xml
  POLICY_NAME_MANUAL_UPDATE_TRIGGER: "aG - %NAME% - Update Trigger"

  POLICY_CATEGORY_INSTALL_TRIGGER: "Manual Install Trigger"
  POLICY_TEMPLATE_INSTALL_TRIGGER: PolicyTemplate-ManualInstallTriggerWithConfig.xml
  SCRIPT_NAME_INSTALL_TRIGGER: 00-UEX-Jamf-Interaction-no-grep.sh
  POLICY_NAME_INSTALL_TRIGGER: "aG - %NAME% - Install Trigger"

  replace_policy: "True"
  replace_script: "False"
  replace_group: "True"

Process:
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfCategoryUploader
    Arguments:
      category_name: "%POLICY_CATEGORY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPackageUploader
    Arguments:
      pkg_category: "%PACKAGE_CATEGORY%"

  - Processor: com.adidas.processors/SMBMounter
    Arguments:
      smb_path: "%SMB_SHARE%"

  - Processor: FileFinder
    Arguments:
      pattern: "%mount_point%/%NAME%/*-CFG.pkg"

  - Processor: URLDownloader
    Comment: "Downloads the file from the source repo to the AutoPkg cache"
    Arguments:
      filename: "%found_basename%"
      url: "file://%found_filename%"
      download_dir: "%RECIPE_CACHE_DIR%/"

  - Processor: com.adidas.processors/SMBUnmounter

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPackageUploader
    Arguments:
      pkg_path: "%RECIPE_CACHE_DIR%/%found_basename%"
      pkg_category: "%PACKAGE_CATEGORY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfScriptUploader
    Arguments:
      script_category: "%SCRIPT_CATEGORY%"
      script_path: "%SCRIPT_NAME_UPDATE%"
      script_priority: "%SCRIPT_PRIORITY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfScriptUploader
    Arguments:
      script_category: "%SCRIPT_CATEGORY%"
      script_path: "%SCRIPT_NAME_INSTALL%"
      script_priority: "%SCRIPT_PRIORITY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfComputerGroupUploader
    Arguments:
      computergroup_template: "%GROUP_TEMPLATE_TARGET%"
      computergroup_name: "%GROUP_NAME_TARGET%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfComputerGroupUploader
    Arguments:
      computergroup_template: "%GROUP_TEMPLATE_EXCLUSION%"
      computergroup_name: "%GROUP_NAME_EXCLUSION%"

  # Creation of policy: aG - %NAME% - Update
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_UPDATE%"
      policy_name: "%POLICY_NAME_UPDATE%"
      icon: "%SELF_SERVICE_ICON%"

  # Creation of policy: aG - %NAME% - Install
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_INSTALL%"
      policy_name: "%POLICY_NAME_INSTALL%"
      icon: "%SELF_SERVICE_ICON%"

  # Creation of policy: aG - %NAME% - Update Cache Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_UPDATE_CACHE_TRIGGER%"
      policy_name: "%POLICY_NAME_UPDATE_CACHE_TRIGGER%"

  # Creation of policy: aG - %NAME% - Cache Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_CACHE_TRIGGER%"
      policy_name: "%POLICY_NAME_CACHE_TRIGGER%"

  # Creation of policy: aG - %NAME% - Install Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_INSTALL_TRIGGER%"
      policy_name: "%POLICY_NAME_INSTALL_TRIGGER%"

  # Creation of policy: aG - %NAME% - Update Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_MANUAL_UPDATE_TRIGGER%"
      policy_name: "%POLICY_NAME_MANUAL_UPDATE_TRIGGER%"