#!/usr/bin/python
#
# Copyright 2020 Lukasz Molenda
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from autopkglib import Processor, ProcessorError

__all__ = ["ReportCreation"]


class ReportCreation(Processor):
    """Create a repot based on the provided information"""

    description = __doc__
    input_variables = {
        "software_name": {
            "required": True,
            "description": "The name of the package with software",
        },
        "version": {
            "required": True,
            "description": "Version of the app",
        },
        "versionbuild": {
            "required": False,
            "description": "Build version of the app",
        },
    }
    output_variables = {
        "report_creator_summary_result": {
            "description": "Description of interesting results."
        },
    }

    def report_app(self):
        """Build a report request and get the summary result"""

        # clear any pre-exising summary result
        if "report_creator_summary_result" in self.env:
            del self.env["report_creator_summary_result"]

        # Return path to pkg.
        if "versionbuild" not in self.env:
            self.env["report_creator_summary_result"] = {
                "summary_text": "The following package was built:",
                "report_fields": ["Application", "Version"],
                "data": {
                    "Application": self.env["software_name"],
                    "Version": self.env["version"],
                },
            }
        else: 
            self.env["report_creator_summary_result"] = {
                "summary_text": "The following package was built:",
                "report_fields": ["Application", "Version", "Build"],
                "data": {
                    "Application": self.env["software_name"],
                    "Version": self.env["version"],
                    "Build": self.env["versionbuild"],
                },
            }

    def main(self):
        """Check if software_name is provided"""
        if self.env.get("software_name"):
            self.report_app()
        else:
            raise ProcessorError("No pkg_path specified.")


if __name__ == "__main__":
    PROCESSOR = ReportCreation()
    PROCESSOR.execute_shell()
