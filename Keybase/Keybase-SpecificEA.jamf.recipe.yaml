Description: Uploads an update/install policy to the Jamf Pro Server and creates a Self Service Policy and Smart Group
Identifier: com.adidas.jamf.Keybase
ParentRecipe: com.adidas.pkg.Keybase
MinimumVersion: "2.3"

Input:
  ## START OF SECTION SPECIFC PER APP - CAN BE MODIFIED ##
  NAME: "Keybase"
  VENDOR_NAME: "Keybase"
  TRIGGER_NAME: "Keybase"
  PACKAGE_CATEGORY: "Applications"
  SCRIPT_CATEGORY: "User Experience"
  SCRIPT_PRIORITY: After
  SELF_SERVICE_ICON: Keybase.png
  SELF_SERVICE_DESCRIPTION: "Keybase is a key directory that maps social media identities to encryption keys (including, but not limited to PGP keys) in a publicly auditable manner. Additionally it offers an end-to-end encrypted chat and cloud storage system, called Keybase Chat and the Keybase Filesystem respectively."
  PARAMETER4_GENERAL_INFO: "%VENDOR_NAME%;%NAME%;%version%"
  PARAMETER5_UEX_OPTIONS: "Quit, Block ssavail"
  PARAMETER6_APP_NAME: "%NAME%"
  PARAMETER7_DURATION: "5"
  ## END OF SECTION SPECIFC PER APP - CAN BE MODIFIED ##

  GROUP_NAME_TARGET: "Application - Installed Version - %NAME%"
  GROUP_TEMPLATE_TARGET: PolicyTemplate-Target-SmartGroup.xml

  POLICY_CATEGORY_UPDATE: "Available Updates"
  POLICY_TEMPLATE_UPDATE: PolicyTemplate-Update.xml
  POLICY_NAME_UPDATE: "aG - %NAME% - Update"
  SCRIPT_NAME_UPDATE: 00-UEX-Update-via-Self-Service.sh
  SELF_SERVICE_DISPLAY_NAME_UPDATE: "%NAME% Update"
  INSTALL_BUTTON_TEXT_UPDATE: Update
  REINSTALL_BUTTON_TEXT_UPDATE: Reinstall

  POLICY_CATEGORY_INSTALL: "Free"
  POLICY_TEMPLATE_INSTALL: PolicyTemplate-Install.xml
  POLICY_NAME_INSTALL: "aG - %NAME% - Install"
  SCRIPT_NAME_INSTALL: 00-UEX-Install-via-Self-Service.sh
  SELF_SERVICE_DISPLAY_NAME_INSTALL: "%NAME% Install"
  INSTALL_BUTTON_TEXT_INSTALL: Get
  REINSTALL_BUTTON_TEXT_INSTALL: Reinstall

  POLICY_CATEGORY_CACHE_TRIGGER: "Manual Cache Trigger"
  POLICY_TEMPLATE_CACHE_TRIGGER: PolicyTemplate-ManualCacheTrigger.xml
  POLICY_NAME_CACHE_TRIGGER: "aG - %NAME% - Cache Trigger"

  POLICY_CATEGORY_UPDATE_CACHE_TRIGGER: "Manual Cache Trigger"
  POLICY_TEMPLATE_UPDATE_CACHE_TRIGGER: PolicyTemplate-ManualUpdateCacheTrigger.xml
  POLICY_NAME_UPDATE_CACHE_TRIGGER: "aG - %NAME% - Update Cache Trigger"

  POLICY_CATEGORY_INSTALL_TRIGGER: "Manual Install Trigger"
  POLICY_TEMPLATE_INSTALL_TRIGGER: PolicyTemplate-ManualInstallTrigger.xml
  SCRIPT_NAME_INSTALL_TRIGGER: 00-UEX-Jamf-Interaction-no-grep.sh
  POLICY_NAME_INSTALL_TRIGGER: "aG - %NAME% - Install Trigger"

  POLICY_CATEGORY_MANUAL_UPDATE_TRIGGER: "Manual Update Trigger"
  POLICY_TEMPLATE_MANUAL_UPDATE_TRIGGER: PolicyTemplate-ManualUpdateTrigger.xml
  POLICY_NAME_MANUAL_UPDATE_TRIGGER: "aG - %NAME% - Update Trigger"

  # Variables for Specific EA flow
  GROUP_NAME_EXCLUSION: "Application - Released Version - %NAME% - SEA - Smart"
  GROUP_NAME_SPECIFIC_EA_STATIC:  "Application - %NAME% - SEA - Static"
  GROUP_TEMPLATE_SMART_GROUP: Template-Exclusion-SmartGroup-SpecificEA.xml
  GROUP_TEMPLATE_STATIC_GROUP: Template-Exclusion-StaticGroup-SpecificEA.xml

  replace_policy: "True"
  replace_script: "False"
  replace_group: "True"

Process:
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPackageUploader
    Arguments:
      pkg_category: "%PACKAGE_CATEGORY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfScriptUploader
    Arguments:
      script_category: "%SCRIPT_CATEGORY%"
      script_path: "%SCRIPT_NAME_UPDATE%"
      script_priority: "%SCRIPT_PRIORITY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfScriptUploader
    Arguments:
      script_category: "%SCRIPT_CATEGORY%"
      script_path: "%SCRIPT_NAME_INSTALL%"
      script_priority: "%SCRIPT_PRIORITY%"

  - Processor: com.github.grahampugh.jamf-upload.processors/JamfComputerGroupUploader
    Arguments:
      computergroup_template: "%GROUP_TEMPLATE_TARGET%"
      computergroup_name: "%GROUP_NAME_TARGET%"

  # Use JamfComputerGroupUploader to upload static group 
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfComputerGroupUploader
    Arguments:
      computergroup_template: "%GROUP_TEMPLATE_STATIC_GROUP%"
      computergroup_name: "%GROUP_NAME_SPECIFIC_EA_STATIC%"
      replace_group: False
      
  # Use JamfComputerGroupUploader to upload smart group 
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfComputerGroupUploader
    Arguments:
      computergroup_template: "%GROUP_TEMPLATE_SMART_GROUP%"
      computergroup_name: "%GROUP_NAME_EXCLUSION%"
      replace_group: False

  # Creation of policy: aG - %NAME% - Update
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_UPDATE%"
      policy_name: "%POLICY_NAME_UPDATE%"
      icon: "%SELF_SERVICE_ICON%"

  # Creation of policy: aG - %NAME% - Install
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_INSTALL%"
      policy_name: "%POLICY_NAME_INSTALL%"
      icon: "%SELF_SERVICE_ICON%"

  # Creation of policy: aG - %NAME% - Update Cache Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_UPDATE_CACHE_TRIGGER%"
      policy_name: "%POLICY_NAME_UPDATE_CACHE_TRIGGER%"

  # Creation of policy: aG - %NAME% - Cache Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_CACHE_TRIGGER%"
      policy_name: "%POLICY_NAME_CACHE_TRIGGER%"

  # Creation of policy: aG - %NAME% - Install Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_INSTALL_TRIGGER%"
      policy_name: "%POLICY_NAME_INSTALL_TRIGGER%"

  # Creation of policy: aG - %NAME% - Update Trigger
  - Processor: com.github.grahampugh.jamf-upload.processors/JamfPolicyUploader
    Arguments:
      policy_template: "%POLICY_TEMPLATE_MANUAL_UPDATE_TRIGGER%"
      policy_name: "%POLICY_NAME_MANUAL_UPDATE_TRIGGER%"
      type_pkg: "%Type%"
      pkg_source: "%Package_Source%"
      recipe_type: "%Recipe%"
      
  - Processor: com.adidas.processors/JamfUploaderTeamsNotifier
    Arguments:
      teams_webhook_url: https://adidasgroup.webhook.office.com/webhookb2/20152856-5e20-4193-a741-fa192e2638ab@3bfeb222-e42c-4535-aace-ea6f7751369b/IncomingWebhook/dc57742e061245d386302e8d5f845cf9/9c865cbe-6e60-42d2-bf0a-31b2621db84e
      #teams_webhook_url: https://adidasgroup.webhook.office.com/webhookb2/20152856-5e20-4193-a741-fa192e2638ab@3bfeb222-e42c-4535-aace-ea6f7751369b/IncomingWebhook/dc57742e061245d386302e8d5f845cf9/9c865cbe-6e60-42d2-bf0a-31b2621db84e
     